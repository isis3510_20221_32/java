package com.example.mimascotta;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mimascotta.checkInterne.Utility.NetworkChangeListener;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;

public class Profile extends AppCompatActivity {

    NetworkChangeListener networkChangeListener = new NetworkChangeListener();

    FirebaseAnalytics analytics;

    private Button backButton;
    private Button delete_card;
    private MaterialButton logoutButton;
    private MaterialButton ChangeCardButton;

    private FirebaseUser user;
    private DatabaseReference referense;
    private String userId;

    private int Banknumerous;

    SharedPreferences sharedPreferences;
    SharedPreferences BankPreference;

    boolean banksave = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        analytics = FirebaseAnalytics.getInstance(this);


        sharedPreferences = getSharedPreferences("SHARED_PREF_USER",MODE_PRIVATE);
        BankPreference = getSharedPreferences("SHARED_BANK_INFO", MODE_PRIVATE);


        banksave = BankPreference.getBoolean("Is_save",false);
        if (banksave){


            Banknumerous = BankPreference.getInt("Bank_Num",0) % 1000;
            TextView TVbank = findViewById(R.id.Payment_info);
            TVbank.setText("**** **** **** *"+Banknumerous);
        }

        user = FirebaseAuth.getInstance().getCurrentUser();
        referense= FirebaseDatabase.getInstance().getReference("User");
        userId = user.getUid();

        final TextView TVemail = findViewById(R.id.Email);
        final TextView TVname = findViewById(R.id.Name);
        final TextView TVfirstname = findViewById(R.id.FirstName);

        referense.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String name = snapshot.child("Name").getValue().toString();
                    String firstname = snapshot.child("Firstname").getValue().toString();
                    String email = snapshot.child("Email").getValue().toString();

                    TVemail.setText("E-Mail: "+email);
                    TVfirstname.setText("Firstname: "+firstname);
                    TVname.setText("Name: "+name);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        backButton = (Button) findViewById(R.id.back_button);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Profile.this,MainView.class);
                startActivity(intent);
                finish();
            }
        });

        delete_card = (Button)findViewById(R.id.delete_button);

        delete_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle = new Bundle();
                bundle.putString("Profil_to_Profil","Delete_card");
                analytics.logEvent("DeleteCard_button",bundle);

                SharedPreferences.Editor editor = BankPreference.edit();
                editor.clear();
                editor.apply();

                Intent intent = new Intent(Profile.this, Profile.class);
                startActivity(intent);
                finish();

            }
        });

        logoutButton = (MaterialButton) findViewById(R.id.logout_button);

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle = new Bundle();
                bundle.putString("Profil_to_LogIn","LogOut");
                analytics.logEvent("LogOut_Button",bundle);

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.clear();
                editor.apply();

                Intent intent = new Intent(Profile.this,MainActivity.class);
                startActivity(intent);
                FirebaseAuth.getInstance().signOut();
                finish();
            }
        });

        ChangeCardButton = (MaterialButton) findViewById(R.id.ChangeCardButton);

        ChangeCardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Profile.this, Change_creditcard.class);
                startActivity(intent);
            }
        });
    }
    @Override
    protected void onStart() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkChangeListener,filter);
        super.onStart();
    }

    @Override
    protected void onStop() {
        unregisterReceiver(networkChangeListener);
        super.onStop();
    }

}