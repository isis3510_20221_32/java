package com.example.mimascotta;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.PackageManagerCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.mimascotta.checkInterne.Utility.NetworkChangeListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.Console;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class new_annoucement extends AppCompatActivity {

    NetworkChangeListener networkChangeListener = new NetworkChangeListener();

    private FirebaseAuth mAuth;
    private DatabaseReference mdatabase;

    private FirebaseStorage storage;
    private StorageReference storageReference;

    private Button backButton;
    private Button pictureButton;
    private MaterialButton sendButton;

    //private MaterialButton sendButton;

    private Uri image = null;
    private Uri url = null;

    private EditText editType;
    private EditText editName;
    private EditText editAge;
    private EditText editLoc;
    private EditText editDesc;

    private String type;
    private String name;
    private String age;
    private String loc;
    private String desc;


    private static final int IMAGE_PICK_CODE = 1000;
    private static final int PERMISSION_CODE = 1001;

    FirebaseAnalytics analytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_annoucement);


        analytics = FirebaseAnalytics.getInstance(this);

        storage = FirebaseStorage.getInstance();
        storageReference= storage.getReference();

        backButton = (Button) findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        pictureButton = (Button) findViewById(R.id.picture_button);
        pictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                    if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_DENIED){
                        String[] permission = {Manifest.permission.READ_EXTERNAL_STORAGE};
                        requestPermissions(permission,PERMISSION_CODE);
                    }
                    else{
                        pickImageFromGallery();
                    }
                }
                else{
                    pickImageFromGallery();
                }

            }

        });

        sendButton = (MaterialButton) findViewById(R.id.Send);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editName = (EditText) findViewById(R.id.animalName);
                editAge = (EditText) findViewById(R.id.animalAge);
                editType= (EditText) findViewById(R.id.animalType);
                editDesc = (EditText) findViewById(R.id.animalDescription);
                editLoc = (EditText) findViewById(R.id.animalLocalisation);
                
                name = editName.getText().toString();
                age = editAge.getText().toString();
                type = editType.getText().toString();
                desc = editDesc.getText().toString();
                loc = editLoc.getText().toString();
                
                if (name.isEmpty() || age.isEmpty() || type.isEmpty() || age.isEmpty() || desc.isEmpty() || loc.isEmpty()){
                    Toast.makeText(new_annoucement.this, "All must be filled", Toast.LENGTH_SHORT).show();
                }
                else {
                    if (Is_int(age)){
                        if (image == null){
                            Toast.makeText(new_annoucement.this,
                                    "You must upload an image of your animal", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            upload();

                            Bundle bundle = new Bundle();
                            bundle.putString("Send_Announce","Announce");
                            analytics.logEvent("Send_Announce",bundle);
                        }
                    }
                    else{
                        Toast.makeText(new_annoucement.this, "Age must be a number", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

    }

    private void pickImageFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_PICK_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_CODE: {
                if (grantResults.length > 0 && grantResults[0] ==
                        PackageManager.PERMISSION_GRANTED) {
                    pickImageFromGallery();
                } else {
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == IMAGE_PICK_CODE) {
            //IMAGE in data.getData()
            image = data.getData();

        }
    }



    @Override
    protected void onStart() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkChangeListener,filter);
        super.onStart();
    }

    @Override
    protected void onStop() {
        unregisterReceiver(networkChangeListener);
        super.onStop();
    }


    private void upload(){

        mAuth = FirebaseAuth.getInstance();
        String id = mAuth.getCurrentUser().getUid();
        final String randomKey = UUID.randomUUID().toString();


        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Uploading ...");
        pd.show();




        StorageReference riversRef = storageReference.child("image").child(randomKey);
        riversRef.putFile(image)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                Task<Uri> downloadUri = taskSnapshot.getStorage().getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        url = task.getResult();



                        /*SharedPreferences ImageUrl = getSharedPreferences("ImageUrl",MODE_PRIVATE);
                        SharedPreferences.Editor editor = ImageUrl.edit();

                        editor.putString("url",url.toString());
                        editor.commit();*/

                        pd.dismiss();
                        Snackbar.make(findViewById(android.R.id.content),"Image Uploaded: "+url.toString(),Snackbar.LENGTH_LONG).show();


                    }
                });
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        pd.dismiss();
                        Toast.makeText(new_annoucement.this, "Upload unsuccessfull", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(@NonNull UploadTask.TaskSnapshot snapshot) {
                        double progressPercent = (100.00 * snapshot.getBytesTransferred()/snapshot.getTotalByteCount());
                        pd.setMessage("Progress :"+ (int)progressPercent + "%");
                    }
                });

        SharedPreferences ImageUrl = getSharedPreferences("ImageUrl",MODE_PRIVATE);


        Map<String, Object> map = new HashMap<>();
        map.put("name",name);
        map.put("type",type);
        map.put("loc",loc);
        map.put("age",age);
        map.put("desc",desc);
        map.put("id",id);
        //map.put("image",ImageUrl.getString("url",""));

        /*SharedPreferences.Editor editor = ImageUrl.edit();
        editor.clear();
        editor.apply();*/


        mdatabase = FirebaseDatabase.getInstance().getReference();
        mdatabase.child("animalAnnounce").child(id).child(randomKey).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(new_annoucement.this, "work", Toast.LENGTH_SHORT).show();
            }
        });

    }
    private boolean Is_int (String str){
        int i = 0;
        while (i < str.length())
        {
            if (str.charAt(i)>'9' || str.charAt(i)<'0'){
                return false;
            }
            i = i+1;
        }
        return true;
    }

}