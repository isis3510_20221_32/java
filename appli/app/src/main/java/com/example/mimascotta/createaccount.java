package com.example.mimascotta;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mimascotta.checkInterne.Utility.NetworkChangeListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


public class createaccount extends AppCompatActivity {

    NetworkChangeListener networkChangeListener = new NetworkChangeListener();

    private Button back;
    private MaterialButton Signup;

    private String email = "";
    private String passowrd = "";
    private String repassowrd = "";
    private String name = "";
    private String firstname = "";

    FirebaseAuth mAuth;
    DatabaseReference mdatabase;

    FirebaseAnalytics analytics;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createaccount);


        analytics = FirebaseAnalytics.getInstance(this);


        TextView editEmail = findViewById(R.id.Email);
        TextView editPassowrd = findViewById(R.id.Password);
        TextView editRepassowrd = findViewById(R.id.Repassword);
        TextView editName = findViewById(R.id.Name);
        TextView editFirstname = findViewById(R.id.FirstName);

        mAuth = FirebaseAuth.getInstance();
        mdatabase = FirebaseDatabase.getInstance().getReference();

        //Buttons
        back = (Button)findViewById(R.id.back_button);
        Signup = (MaterialButton) findViewById(R.id.Signup_Button);

        //admin

        //Clic back button
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //Clic Sign Up
        Signup.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View view) {


                name = editName.getText().toString();
                passowrd= editPassowrd.getText().toString();
                repassowrd = editRepassowrd.getText().toString();
                email = editEmail.getText().toString();
                firstname = editFirstname.getText().toString();

                if (!name.isEmpty() && !passowrd.isEmpty() && !repassowrd.isEmpty() && !email.isEmpty() && !firstname.isEmpty())
                {
                    if (passowrd.length() >= 6) {
                        if (passowrd.equals( repassowrd)) {
                            registerUser();
                            finish();
                        } else {
                            Toast.makeText(createaccount.this, "The passowrd is not the same in the two lines", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        Toast.makeText(createaccount.this, "The passowrd must contain at least 6 characters", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(createaccount.this,"All the lines must be completed",Toast.LENGTH_SHORT).show();
                }
            }
        });




    }

    private void registerUser() {
        mAuth.createUserWithEmailAndPassword(email,passowrd).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful())
                {
                    Map<String, Object> map = new HashMap<>();
                    map.put("Firstname",firstname);
                    map.put("Name",name);
                    map.put("Email",email);
                    map.put("Passowrd",passowrd);


                    String id = mAuth.getCurrentUser().getUid();

                    mdatabase.child("User").child(id).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task2) {
                            if (task2.isSuccessful())
                            {
                                //End of the inscription
                                finish();
                            }
                            else
                            {
                                Toast.makeText(createaccount.this, "An error hapenned: code 2", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
                else
                {
                    Toast.makeText(createaccount.this, "An error happened:code 1", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    @Override
    protected void onStart() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkChangeListener,filter);
        super.onStart();
    }

    @Override
    protected void onStop() {
        unregisterReceiver(networkChangeListener);
        super.onStop();
    }
}