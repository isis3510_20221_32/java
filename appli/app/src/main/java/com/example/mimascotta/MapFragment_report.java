package com.example.mimascotta;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationRequest;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class MapFragment_report extends Fragment {


    String pos = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("REPORT_POSITION", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();



        //Initialise view
        View view = inflater.inflate(R.layout.fragment_map_report,container,false);

        //Initialise map fragment
        SupportMapFragment supportMapFragment = (SupportMapFragment)
                getChildFragmentManager().findFragmentById(R.id.map_report);

        //Async map
        supportMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull GoogleMap googleMap) {
                //when map is loaded
                googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(@NonNull LatLng latLng) {
                        //When clicked on map
                        //Initialise the picker
                        MarkerOptions markerOptions = new MarkerOptions();
                        //set position of the marker
                        markerOptions.position(latLng);
                        //set title
                        pos = latLng.latitude + " : " +latLng.longitude;
                        markerOptions.title(pos);
                        editor.putString("pos_report",pos);
                        editor.putBoolean("pos_save",true);
                        editor.commit();
                        //Remove all markers
                        googleMap.clear();
                        //animating to zoom the marker
                        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                                latLng,10
                        ));
                        //Add marker
                        googleMap.addMarker(markerOptions);
                    }
                });
            }
        });



        return view;
    }


}