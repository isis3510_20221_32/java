package com.example.mimascotta;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mimascotta.checkInterne.Utility.NetworkChangeListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Report_animals extends AppCompatActivity{

    NetworkChangeListener networkChangeListener =new NetworkChangeListener();
    private Button backbutton;
    private MaterialButton validation;

    private EditText editType;
    private EditText editDesc;

    SharedPreferences sharedPreferences;
    DatabaseReference mdatabase;
    FirebaseAuth mAuth;

    FirebaseAnalytics analytics;



    String type = "";
    String desc = "";
    String pos ="";

    private Fragment fragment = new MapFragment_report();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_animals);

        analytics = FirebaseAnalytics.getInstance(this);

        mdatabase = FirebaseDatabase.getInstance().getReference();
        sharedPreferences= getSharedPreferences("REPORT_POSITION",MODE_PRIVATE);
        mAuth = FirebaseAuth.getInstance();


        //open fragment
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_layout_report,fragment)
                .commit();


        backbutton = (Button)findViewById(R.id.back_button);

        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        validation = (MaterialButton)findViewById(R.id.report_button_validation);
        validation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!sharedPreferences.getBoolean("pos_save",false)){
                    Toast.makeText(Report_animals.this, "No position detected", Toast.LENGTH_SHORT).show();
                }
                else {
                    editType = (EditText) findViewById(R.id.Animal_Type);
                    editDesc = (EditText) findViewById(R.id.Animal_description);

                    type = editType.getText().toString();
                    desc = editDesc.getText().toString();
                    if (type.isEmpty() || desc.isEmpty())
                    {
                        Toast.makeText(Report_animals.this, "must fill all the formula", Toast.LENGTH_SHORT).show();
                    }
                    else {

                        Bundle bundle = new Bundle();
                        bundle.putString("Report_to_Report","SendReport");
                        analytics.logEvent("SendReport_Button",bundle);

                        pos = sharedPreferences.getString("pos_report", "");

                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.clear();
                        editor.apply();

                        Map<String, Object> map = new HashMap<>();
                        map.put("Animal_Type",type);
                        map.put("Animal_desc",desc);
                        map.put("Animal_pos",pos);

                        final String randomKey = UUID.randomUUID().toString();


                        mdatabase.child("Animals_Report").child(randomKey).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Intent intent = new Intent(Report_animals.this,Report_animals.class);
                                startActivity(intent);
                                finish();
                            }
                        });
                    }


                }



            }
        });


    }
    @Override
    protected void onStart() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkChangeListener,filter);
        super.onStart();
    }

    @Override
    protected void onStop() {
        unregisterReceiver(networkChangeListener);
        super.onStop();
    }


}