package com.example.mimascotta;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.renderscript.ScriptGroup;
import android.view.View;
import android.view.inputmethod.InputBinding;
import android.widget.Button;

import com.example.mimascotta.checkInterne.Utility.NetworkChangeListener;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;

import java.util.ArrayList;

public class Sell_activity extends AppCompatActivity {

    NetworkChangeListener networkChangeListener = new NetworkChangeListener();
    private FirebaseAuth mAuth;
    private Button backButton;
    private MaterialButton AnnounceButton;
    ArrayList<Announce> list;
    MyAdapter myAdapter;
    RecyclerView recyclerView;
    DatabaseReference database;
    FirebaseStorage mStorage;

    FirebaseAnalytics analytics;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell);
        backButton = (Button) findViewById(R.id.back_button);

        analytics = FirebaseAnalytics.getInstance(this);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



        AnnounceButton = (MaterialButton) findViewById(R.id.AnnounceButton);
        AnnounceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Sell_activity.this, new_annoucement.class);
                startActivity(intent);
            }
        });


        recyclerView = findViewById(R.id.MyAnnounce);
        mStorage = FirebaseStorage.getInstance();
        database = FirebaseDatabase.getInstance().getReference("animalAnnounce");
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        list = new ArrayList<>();
        myAdapter = new MyAdapter(this,list);
        recyclerView.setAdapter(myAdapter);

        database.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                mAuth = FirebaseAuth.getInstance();
                String id = mAuth.getCurrentUser().getUid();
                DataSnapshot dataSnapshot = snapshot.child(id);
                    for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()){
                        Announce announce = dataSnapshot2.getValue(Announce.class);
                        list.add(announce);
                    }
                myAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }


    @Override
    protected void onStart() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkChangeListener,filter);
        super.onStart();
    }

    @Override
    protected void onStop() {
        unregisterReceiver(networkChangeListener);
        super.onStop();
    }
}