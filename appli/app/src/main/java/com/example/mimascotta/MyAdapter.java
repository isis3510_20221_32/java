package com.example.mimascotta;

import android.content.Context;
import android.net.Uri;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    Context context;

    ArrayList<Announce> list;

    public MyAdapter(Context context, ArrayList<Announce> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item,parent,false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Announce announce = list.get(position);
        holder.TVtype.setText(announce.getType());
        holder.TVage.setText(announce.getAge());
        holder.TVname.setText(announce.getName());

        //String imageUri = null;

        //imageUri = announce.getImage();

        //Picasso.get().load(imageUri).into(holder.IV);



    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView TVtype,TVname,TVage;
        //ImageView IV;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            TVtype = itemView.findViewById(R.id.typeText);
            TVname =  itemView.findViewById(R.id.nameText);
            TVage = itemView.findViewById(R.id.ageText);
            //IV = itemView.findViewById(R.id.animalPicture);
        }
    }
}
