package com.example.mimascotta;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mimascotta.checkInterne.Utility.NetworkChangeListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    NetworkChangeListener networkChangeListener = new NetworkChangeListener();

    private FirebaseAuth mAuth;
    FirebaseAnalytics analytics;
    SharedPreferences sharedPreferences;

    private CheckBox rememberme;
    boolean isrememberd = false;

    private TextView Loginemail;
    private TextView Loginpassowrd;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //initialise firebase auth
        mAuth = FirebaseAuth.getInstance();

        analytics = FirebaseAnalytics.getInstance(this);




        //Buttons
        MaterialButton login = (MaterialButton) findViewById(R.id.loginbutton);
        Button Createaccount = (Button)findViewById(R.id.createaccount);

        rememberme = (CheckBox)findViewById(R.id.Rememberme);

        sharedPreferences = getSharedPreferences("SHARED_PREF_USER", MODE_PRIVATE);

        isrememberd = sharedPreferences.getBoolean("Check_Box",false);





        //admin


        if (mAuth.getCurrentUser() != null)
        {
            if (!isrememberd) {
                FirebaseAuth.getInstance().signOut();
            }
            else{
                showMainView();
            }
        }




        Createaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, createaccount.class);
                startActivity(intent);
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //clic on login
                authenticateuser();
            }


        });


    }

    private void authenticateuser()
    {

        Loginemail = findViewById(R.id.Email);
        Loginpassowrd = findViewById(R.id.password);

        String email = Loginemail.getText().toString();
        String passowrd = Loginpassowrd.getText().toString();



        if (email.isEmpty() || passowrd.isEmpty())
        {
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show();
            return;
        }
        mAuth.signInWithEmailAndPassword(email,passowrd)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful())
                        {
                            //Sign in success
                            boolean checked = rememberme.isChecked();

                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("Email",email);
                            editor.putString("Passowrd",passowrd);
                            editor.putBoolean("Check_Box",checked);

                            editor.apply();
                            showMainView();
                            Toast.makeText(MainActivity.this, "Authentification succesfull", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            //Sign fail
                            Toast.makeText(MainActivity.this, "Authentification failed", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }

    private void showMainView()
    {
        Bundle bundle = new Bundle();
        bundle.putString("Connection","Connection_Successfull");
        analytics.logEvent("Log_in_Button",bundle);

        Intent intent = new Intent(this,MainView.class);
        startActivity(intent);
        finish();

    }

    @Override
    protected void onStart() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkChangeListener,filter);
        super.onStart();
    }

    @Override
    protected void onStop() {
        unregisterReceiver(networkChangeListener);
        super.onStop();
    }
}