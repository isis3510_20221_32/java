package com.example.mimascotta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mimascotta.checkInterne.Utility.NetworkChangeListener;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.analytics.FirebaseAnalytics;

public class Change_creditcard extends AppCompatActivity {

    NetworkChangeListener networkChangeListener = new NetworkChangeListener();

    FirebaseAnalytics analytics;

    private Button backButton;
    private Button validationButton;

    private String CardNumerous ="";
    private String OwnerName ="";
    private String OwnerFirstName ="";
    private String CardCryptogram ="";
    private EditText EditCN,EditCr, EditN,EditFN;

    SharedPreferences Bankpreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_creditcard);

        analytics = FirebaseAnalytics.getInstance(this);


        backButton = (Button)findViewById(R.id.back_button);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Bankpreference = getSharedPreferences("SHARED_BANK_INFO",MODE_PRIVATE);

        validationButton = (MaterialButton)findViewById(R.id.ValidateButton);

        validationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences.Editor editor = Bankpreference.edit();

                EditCN = findViewById(R.id.Numerous_card);
                EditCr = findViewById(R.id.Cryptogram_card);
                EditFN = findViewById(R.id.FirstName_card);
                EditN = findViewById(R.id.Name_card);

                CardNumerous = EditCN.getText().toString();
                CardCryptogram = EditCr.getText().toString();
                OwnerName = EditN.getText().toString();
                OwnerFirstName = EditFN.getText().toString();

                if (!Is_int(CardCryptogram) || !Is_int(CardNumerous)) {
                    Toast.makeText(Change_creditcard.this, "Invalid card", Toast.LENGTH_SHORT).show();
                }
                else {

                    Bundle bundle = new Bundle();
                    bundle.putString("ChangeCard_to_Profil","Profil_access");
                    analytics.logEvent("SaveCard_Button",bundle);


                    editor.putInt("Bank_Num",new Integer(CardNumerous));
                    editor.putInt("Bank_Crypto",new Integer(CardCryptogram));
                    editor.putBoolean("Is_save",true);
                    editor.putString("Name_Owner",OwnerName);
                    editor.putString("Fname_Owner",OwnerFirstName);
                    editor.commit();


                    finish();
                }



            }
        });
    }

    @Override
    protected void onStart() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkChangeListener,filter);
        super.onStart();
    }

    @Override
    protected void onStop() {
        unregisterReceiver(networkChangeListener);
        super.onStop();
    }

    private boolean Is_int (String str){
        int i = 0;
        while (i < str.length())
        {
            if (str.charAt(i)>'9' || str.charAt(i)<'0'){
                return false;
            }
            i = i+1;
        }
        return true;
    }
}