package com.example.mimascotta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mimascotta.checkInterne.Utility.NetworkChangeListener;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.analytics.FirebaseAnalytics;

public class donationActivity extends AppCompatActivity {


    FirebaseAnalytics analytics;

    private NetworkChangeListener networkChangeListener = new NetworkChangeListener();

    private Button backButton;
    private MaterialButton sendButton;

    private EditText ETamount;

    private TextView TVcard;
    private TextView TVtotal;

    private String card;
    private int total = 0;

    private boolean can;

    private SharedPreferences BankPreference;
    private SharedPreferences DonationPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donation);

        analytics = FirebaseAnalytics.getInstance(this);


        DonationPreference = getSharedPreferences("SHARED_DONATION", MODE_PRIVATE);
        total = DonationPreference.getInt("total",0);
        if (total != 0){
            TVtotal = (TextView) findViewById(R.id.totalAmount);
            TVtotal.setText(total+"$");
        }


        backButton = (Button) findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        sendButton = (MaterialButton)findViewById(R.id.sendButton);

        BankPreference = getSharedPreferences("SHARED_BANK_INFO", MODE_PRIVATE);

        can = BankPreference.getBoolean("Is_save",false);
        TVcard = (TextView) findViewById(R.id.cardMessage);
        card = TVcard.getText().toString();

        if (!can){
            TVcard.setText(card + "no card save");
            sendButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(donationActivity.this, "You need to save a card before", Toast.LENGTH_SHORT).show();
                }
            });

        }
        else {
            TVcard.setText(card +"*** *** *** "+ BankPreference.getInt("Bank_Num",0)%1000);
            sendButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ETamount = (EditText) findViewById(R.id.amount);
                    SharedPreferences.Editor editor = DonationPreference.edit();
                    total =  total + new Integer(ETamount.getText().toString());
                    editor.putInt("total",total);
                    editor.commit();

                    TVtotal = (TextView) findViewById(R.id.totalAmount);
                    TVtotal.setText(total+"$");

                    Bundle bundle = new Bundle();
                    bundle.putString("Made_Donation","Donation");
                    analytics.logEvent("Made_Donation",bundle);
                }
            });

        }

    }

    @Override
    protected void onStart() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkChangeListener,filter);
        super.onStart();
    }

    @Override
    protected void onStop() {
        unregisterReceiver(networkChangeListener);
        super.onStop();
    }
}