package com.example.mimascotta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.mimascotta.checkInterne.Utility.NetworkChangeListener;
import com.google.firebase.analytics.FirebaseAnalytics;

public class MainView extends AppCompatActivity {

    NetworkChangeListener networkChangeListener = new NetworkChangeListener();

    FirebaseAnalytics analytics;

    //Button
    private Button profil;
    private ImageButton ReportButton;
    private ImageButton adopt;
    private ImageButton sell;
    private ImageButton donation;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_view);

        analytics = FirebaseAnalytics.getInstance(this);


        ReportButton = (ImageButton) findViewById(R.id.report_button);
        adopt = (ImageButton) findViewById(R.id.adoption_button);
        sell = (ImageButton)findViewById(R.id.sell_button);

        ReportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putString("Main_to_report","access_report_page");
                analytics.logEvent("report_button",bundle);

                Intent intent = new Intent(MainView.this, Report_animals.class);
                startActivity(intent);
            }
        });



        adopt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("Main_to_adopt","access_adopt_page");
                analytics.logEvent("adopt_button",bundle);

                Intent intent =new Intent(MainView.this, announcelist.class);
                startActivity(intent);

            }
        });

        sell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("Main_to_sell","access_sell_page");
                analytics.logEvent("sell_button",bundle);

                Intent intent =new Intent(MainView.this,Sell_activity.class);
                startActivity(intent);
            }
        });

        profil = (Button) findViewById(R.id.profil_button);
        profil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle = new Bundle();
                bundle.putString("Main_to_profil","Profil_access");
                analytics.logEvent("Profil_button",bundle);

                Intent intent = new Intent(MainView.this, Profile.class);
                startActivity(intent);
                finish();
            }
        });

        donation = (ImageButton) findViewById(R.id.donation_button);
        donation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainView.this, donationActivity.class);
                startActivity(intent);
            }
        });

    }
    @Override
    protected void onStart() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkChangeListener,filter);
        super.onStart();
    }

    @Override
    protected void onStop() {
        unregisterReceiver(networkChangeListener);
        super.onStop();
    }
}